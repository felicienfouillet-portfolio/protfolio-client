import { Component, Input, OnInit } from '@angular/core';
import { IProject } from 'src/app/models/projects/projects.model';

@Component({
  selector: 'app-project-card',
  templateUrl: './project-card.component.html',
  styleUrls: ['./project-card.component.scss']
})
export class ProjectCardComponent implements OnInit {

  @Input() project: IProject = {
    _id: 0,
    title: 'Project Title',
    subtitle: 'Project Subtitle',
    imageUri: 'https://material.angular.io/assets/img/examples/shiba2.jpg',
    description: 'Project Description'
  };

  constructor() { }

  ngOnInit(): void {
  }
}
