import { Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { SettingsService } from '@felicienfouillet/ngx-csf-core';
import { take } from 'rxjs';
import { IProject } from 'src/app/models/projects/projects.model';
import { ProjectsService } from 'src/app/services/projects/projects.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  public projects: Array<IProject> = new Array<IProject>();

  get cols(): number {
    if (this._settingsService.currentBreakPoint === Breakpoints.XLarge) return 3;
    if (this._settingsService.currentBreakPoint === Breakpoints.Large) return 3;
    if (this._settingsService.currentBreakPoint === Breakpoints.Medium) return 2;
    if (this._settingsService.currentBreakPoint === Breakpoints.Small) return 1;
    if (this._settingsService.currentBreakPoint === Breakpoints.XSmall) return 1;
    return 1;
  }

  constructor(private readonly _settingsService: SettingsService, private readonly _projectsService: ProjectsService) { }

  ngOnInit(): void {
    this.updateProjectList();
  }

  updateProjectList(): void {
    this._projectsService.get({
      apiUrl: '/projects'
    }).pipe(take(1)).subscribe((result: any) => {
      this.projects = result;
    });
  }
}
