import { Component, Input, OnInit } from '@angular/core';
import { IProject } from 'src/app/models/projects/projects.model';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  @Input() project: IProject = {
    _id: 0,
    title: 'Project Title',
    subtitle: 'Project Subtitle',
    imageUri: 'https://material.angular.io/assets/img/examples/shiba2.jpg',
    description: 'Project Description'
  };

  constructor() { }

  ngOnInit(): void {
  }

}
