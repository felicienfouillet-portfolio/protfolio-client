import { Breakpoints } from '@angular/cdk/layout';
import { AfterViewInit, Component, HostListener, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SettingsService } from '@felicienfouillet/ngx-csf-core';
import { SkillsDialogData } from 'src/app/models/resume/resume.models';
import { SkillsDetailsDialogComponent } from './skills-details-dialog/skills-details-dialog.component';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.scss']
})
export class ResumeComponent implements OnInit, AfterViewInit {

  get isMobile(): boolean {
    return this._settingsService.mediaScreenInfos.isMobile;
  }

  get isDarkMode(): boolean {
    return this._settingsService.getProperty('general-isDarkMode') as boolean;
  }

  get currentBreakPoint(): string {
    return this._settingsService.currentBreakPoint;
  }

  get cols(): number {
    if (this._settingsService.currentBreakPoint === Breakpoints.XLarge) return 6;
    if (this._settingsService.currentBreakPoint === Breakpoints.Large) return 6;
    if (this._settingsService.currentBreakPoint === Breakpoints.Medium) return 6;
    if (this._settingsService.currentBreakPoint === Breakpoints.Small) return 1;
    if (this._settingsService.currentBreakPoint === Breakpoints.XSmall) return 1;
    return 1;
  }

  get colSpans(): any {
    const values = [['profilePicture', 2], ['bio', 4], ['trainExp', 4], ['skills', 2], ['interests', 6]];
    const colSpans = {};

    values.forEach(value => {
      colSpans[value[0]] = this.cols;

      if (this.cols > 1) {
        colSpans[value[0]] = value[1];
      }
    });

    return colSpans;
  }

  get rows(): any {
    const gridComponentsHeights = this.getGridComponentsHeights();
    const rows = {};

    this.componentsIds.forEach((componentId, index) => {
      rows[componentId] = gridComponentsHeights[index];
    });

    return rows;
  }

  private readonly componentsIds = ['profilePicture', 'bio', 'trainExp', 'skills']; //, 'interests'
  private readonly componentsGridByIds = [
    ['profilePicture', 'bio'],
    ['trainExp', 'skills'],
    // ['interests']
  ];

  private gridComponentsHeights: Array<number> = new Array<number>();

  constructor(private readonly _settingsService: SettingsService, private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.gridComponentsHeights = this.calculateGridComponentsHeights();
    }, 250);
  }

  @HostListener('window:resize')
  onResize() {
    this.gridComponentsHeights = this.calculateGridComponentsHeights();
  }

  public showSkillsDetails(skillId: string): void {
    const showDetails = {
      'angular': () => {
        this.openSkillsDetailsDialog({
          title: 'Angular',
          description: 'Angular is a JavaScript-based open-source front-end web application platform '
        });
      },
      'typescript': () => {
        this.openSkillsDetailsDialog({
          title: 'TypeScript',
          description: 'TypeScript is a typed superset of JavaScript that compiles to plain JavaScript.'
        });
      },
      'javascript': () => {
        this.openSkillsDetailsDialog({
          title: 'JavaScript',
          description: 'JavaScript is a high-level, interpreted programming language. It is a language which is also characterized as dynamic, weakly typed, prototype-based and multi-paradigm.'
        });
      },
      'node': () => {
        this.openSkillsDetailsDialog({
          title: 'Node.js',
          description: 'Node.js is an open-source, cross-platform JavaScript run-time environment that executes JavaScript code outside a web browser.'
        });
      },
      'c_sharp': () => {
        this.openSkillsDetailsDialog({
          title: 'C#',
          description: 'C# is a multi-paradigm programming language encompassing strong typing, imperative, declarative, functional, generic, object-oriented (class-based), and component-oriented programming disciplines.'
        });
      },
      'ethereum': () => {
        this.openSkillsDetailsDialog({
          title: 'Ethereum',
          description: 'Ethereum is a decentralized platform that runs smart contracts: applications that run exactly as programmed without any possibility of downtime, censorship, fraud or third-party interference.'
        });
      },
      'solidity': () => {
        this.openSkillsDetailsDialog({
          title: 'Solidity',
          description: 'Solidity is a programming language for smart contracts. Solidity is a prototype-based programming language that lets you write contracts in a declarative language.'
        });
      },
      'godot': () => {
        this.openSkillsDetailsDialog({
          title: 'Godot Engine',
          description: 'Godot is a free and open-source game engine and game development environment.'
        });
      }
    }[skillId];

    showDetails();
  }

  private openSkillsDetailsDialog(data: SkillsDialogData): void {
    this.dialog.open(SkillsDetailsDialogComponent, {
      data: data
    });
  }

  private getGridComponentsHeights(): any {
    if (this.cols === 1) {
      const gridComponentsHeight: Array<number> = new Array<number>();

      this.componentsIds.forEach(id => {
        gridComponentsHeight.push(Math.round(this.getComponentHeight(id) + 64));
      });

      return gridComponentsHeight;
    }

    return this.gridComponentsHeights;
  }

  private getComponentHeight(id: string): number {
    const component: HTMLDivElement = document.getElementById(id) as HTMLDivElement;
    return component.getBoundingClientRect().height;
  }

  // calculate minimum components height for each row of the grid based on the comparaison of components heights
  private calculateGridComponentsHeights(): Array<number> {
    const gridComponentsHeight: Array<number> = new Array<number>();

    this.componentsGridByIds.forEach(components => {
      const componentsHeights = new Array<number>();

      components.forEach(componentId => {
        componentsHeights.push(Math.round(this.getComponentHeight(componentId) + 64));
      });

      const mappedComponentsHeights = componentsHeights.map(componentHeight => {
        const minComponentHeight = Math.round(Math.max(...componentsHeights));

        if (componentHeight < minComponentHeight) {
          return minComponentHeight;
        }

        return componentHeight;
      });

      gridComponentsHeight.push(...mappedComponentsHeights);
    });

    return gridComponentsHeight;
  }
}
