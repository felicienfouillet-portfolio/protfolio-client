import { Component, OnInit } from '@angular/core';
import { SettingsService } from '@felicienfouillet/ngx-csf-core';
import { take } from 'rxjs';
import { TrainingsService } from 'src/app/services/trainings/trainings.service';

@Component({
  selector: 'app-trainings',
  templateUrl: './trainings.component.html',
  styleUrls: ['./trainings.component.scss']
})
export class TrainingsComponent implements OnInit {

  public trainings: Array<any>;

  get isDarkMode(): boolean {
    return this._settingsService.getProperty('general-isDarkMode') as boolean;
  }

  constructor(private readonly _settingsService: SettingsService, private readonly _trainingsService: TrainingsService) { }

  ngOnInit(): void {
    this.updateTrainingList();
  }

  updateTrainingList(): void {
    this._trainingsService.get({
      apiUrl: '/trainings'
    }).pipe(take(1)).subscribe((result: any) => {
      this.trainings = result;
    });
  }
}
