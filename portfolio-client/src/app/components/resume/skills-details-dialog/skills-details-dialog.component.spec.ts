import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillsDetailsDialogComponent } from './skills-details-dialog.component';

describe('SkillsDetailsDialogComponent', () => {
  let component: SkillsDetailsDialogComponent;
  let fixture: ComponentFixture<SkillsDetailsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkillsDetailsDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillsDetailsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
