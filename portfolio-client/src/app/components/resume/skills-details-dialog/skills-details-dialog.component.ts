import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SkillsDialogData } from 'src/app/models/resume/resume.models';

@Component({
  selector: 'app-skills-details-dialog',
  templateUrl: './skills-details-dialog.component.html',
  styleUrls: ['./skills-details-dialog.component.scss']
})
export class SkillsDetailsDialogComponent implements OnInit {

  public title: string;
  public description: string;

  constructor(public dialogRef: MatDialogRef<SkillsDetailsDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: SkillsDialogData) { }

  ngOnInit(): void {
    this.title = this.data?.title;
    this.description = this.data?.description;
  }

}
