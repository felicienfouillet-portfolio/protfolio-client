import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'resume-card',
  templateUrl: './resume-card.component.html',
  styleUrls: ['./resume-card.component.scss']
})
export class ResumeCardComponent implements OnInit {
  @Input()
  public title: string;

  @Input()
  public subtitle: string;

  @Input()
  public startDate: string;

  @Input()
  public endDate: string;

  @Input()
  public description: string;

  constructor() { }

  ngOnInit(): void {
  }

}
