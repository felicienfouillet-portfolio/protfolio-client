import { Component, Input, OnInit } from '@angular/core';
import { SettingsService } from '@felicienfouillet/ngx-csf-core';

@Component({
  selector: 'circular-skill',
  templateUrl: './circular-skill.component.html',
  styleUrls: ['./circular-skill.component.scss']
})
export class CircularSkillComponent implements OnInit {

  get isDarkMode(): boolean {
    return this._settingsService.getProperty('general-isDarkMode') as boolean;
  }

  @Input() width: string = '150px';
  @Input() value: number = 0;

  get strokeDashoffset(): string {
    return `${420 - ((this.value * 420) / 100)}`;
  }

  constructor(private readonly _settingsService: SettingsService) { }

  ngOnInit(): void {
  }

}
