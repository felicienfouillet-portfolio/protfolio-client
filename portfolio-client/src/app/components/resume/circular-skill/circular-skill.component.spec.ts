import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CircularSkillComponent } from './circular-skill.component';

describe('CircularSkillComponent', () => {
  let component: CircularSkillComponent;
  let fixture: ComponentFixture<CircularSkillComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CircularSkillComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CircularSkillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
