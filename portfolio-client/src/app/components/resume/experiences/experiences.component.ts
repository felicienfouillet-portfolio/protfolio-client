import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs';
import { ExperiencesService } from 'src/app/services/experiences/experiences.service';

@Component({
  selector: 'app-experiences',
  templateUrl: './experiences.component.html',
  styleUrls: ['./experiences.component.scss']
})
export class ExperiencesComponent implements OnInit {
  public experiences: Array<any>;

  constructor(private readonly _experiencesService: ExperiencesService) { }

  ngOnInit(): void {
    this.updateExperienceList();
  }

  private updateExperienceList(): void {
    this._experiencesService.get({
      apiUrl: '/experiences'
    }).pipe(take(1)).subscribe((result: any) => {
      this.experiences = result;
    });
  }
}
