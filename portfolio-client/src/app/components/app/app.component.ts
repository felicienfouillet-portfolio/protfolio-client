import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { SettingsService } from '@felicienfouillet/ngx-csf-core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  public isLoading: boolean = false;

  public socialIconsStatus = {
    linkedin: false,
    phone: false,
    mail: false,
    gitlab: false,
    github: false
  }

  get isMobile(): boolean {
    return this._settingsService.mediaScreenInfos.isMobile;
  }

  get isDarkMode(): boolean {
    return this._settingsService.getProperty('general-isDarkMode') as boolean;
  }
  set isDarkMode(isDarkMode: boolean) {
    this._settingsService.setProperty('general-isDarkMode', isDarkMode);
    this._settingsService.saveStore();
    this._settingsService.changeTheme(isDarkMode);
  }

  private readonly destroy$: Subject<void> = new Subject<void>();

  constructor(
    private readonly _settingsService: SettingsService,
    private readonly iconRegistry: MatIconRegistry,
    private readonly sanitizer: DomSanitizer
  ) {
    this.isLoading = true;
    this.iconRegistry.addSvgIcon('linkedin', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/linkedin.svg'));
    this.iconRegistry.addSvgIcon('gitlab', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/gitlab.svg'));
    this.iconRegistry.addSvgIcon('github', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/github.svg'));
  }

  ngOnInit(): void {
    this.isLoading = false;
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public redirect(url: string) {
    window.open(url, '_blank');
  }
}
