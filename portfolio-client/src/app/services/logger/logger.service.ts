import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { LoggerOption, LogTypes } from '../../models/logger/logger.model';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  /**
   * 
   * @param message 
   * @param optionalParams 
   */
  public static log(message?: any, ...optionalParams: any[]): void {
    if (!environment.production) console.log(message, ...optionalParams);
  }

  /**
   * 
   * @param message 
   * @param optionalParams 
   */
  public static info(message?: any, ...optionalParams: any[]): void {
    if (!environment.production) console.info(message, ...optionalParams);
  }

  /**
   * 
   * @param message 
   * @param optionalParams 
   */
  public static warn(message?: any, ...optionalParams: any[]): void {
    if (!environment.production) console.warn(message, ...optionalParams);
  }

  /**
   * 
   * @param message 
   * @param optionalParams 
   */
  public static error(message?: any, ...optionalParams: any[]): void {
    if (!environment.production) console.error(message, ...optionalParams);
  }

  /**
   * 
   * @param tabularData 
   * @param properties 
   */
  public static table(tabularData: any, properties?: ReadonlyArray<string>): void {
    if (!environment.production) console.table(tabularData, properties);
  }

  /**
   * 
   * @param label 
   */
  public static time(label?: string): void {
    if (!environment.production) console.time(label);
  }

  /**
   * 
   * @param message 
   * @param optionalParams 
   */
  public static trace(message?: any, ...optionalParams: any[]): void {
    if (!environment.production) console.trace(message, ...optionalParams);
  }

  public static clear(): void {
    if (!environment.production) console.clear();
  }

  constructor() { }

  /**
   * log data
   * @param data any
   * @param type default: null
   */
  public static notify(data: LoggerOption, type: LogTypes = LogTypes.DEFAULT): void {
    data.prefix = data.prefix ? data.prefix : '';

    const formattedPrefix = {
      'error': {
        message: '%cError',
        styles: 'background-color: #e0005a; color: #ffffff; font-weight: bold; padding: 0 .5em;'
      },
      'warn': {
        message: '%cWarning',
        styles: 'background-color: gold; color: #ffffff; font-weight: bold; padding: 0 .5em;'
      },
      'service': {
        message: `%c${data.prefix}`,
        styles: 'background-color: green; color: #ffffff; font-weight: bold; padding: 0 .5em;'
      },
      'default': {
        message: `%c${data.prefix}`,
        styles: 'background-color: #ffffff; color: #000000; font-weight: bold; padding: 0 .5em;'
      }
    }[type];

    if (type === LogTypes.ERROR) {
      console.groupCollapsed(
        formattedPrefix.message,
        formattedPrefix.styles,
        data.title);

      if (data.data instanceof Array) {
        data.data.forEach(message => {
          console.error(message ? message : '');
        });

        console.groupEnd();
        return;
      }

      console.error((data as LoggerOption).data ? (data as LoggerOption).data : '');

      console.groupEnd();
      return;
    }

    if (type === LogTypes.WARN) {
      console.groupCollapsed(
        formattedPrefix.message,
        formattedPrefix.styles,
        data.title);

      if (data.data instanceof Array) {
        data.data.forEach(message => {
          console.warn(message ? message : '');
        });

        console.groupEnd();
        return;
      }

      console.warn((data as LoggerOption).data ? (data as LoggerOption).data : '');

      console.groupEnd();
      return;
    }

    if (type === LogTypes.SERVICE) {
      console.log(formattedPrefix.message, formattedPrefix.styles, data.title);
      return;
    }

    console.groupCollapsed(
      formattedPrefix.message,
      formattedPrefix.styles,
      data.title);

    if (data.data instanceof Array) {
      data.data.forEach(message => {
        console.log(message ? message : '');
      });

      console.groupEnd();
      return;
    }

    console.log((data as LoggerOption).data ? (data as LoggerOption).data : '');
    console.groupEnd();
  }
}

