import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { ExperiencesComponent } from './components/resume/experiences/experiences.component';
import { ResumeCardComponent } from './components/resume/resume-card/resume-card.component';
import { ResumeComponent } from './components/resume/resume.component';
import { TrainingsComponent } from './components/resume/trainings/trainings.component';
import { MaterialModule } from './modules/material/material.module';
import { CircularSkillComponent } from './components/resume/circular-skill/circular-skill.component';
import { SkillsDetailsDialogComponent } from './components/resume/skills-details-dialog/skills-details-dialog.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { ProjectComponent } from './components/projects/project/project.component';
import { BlogComponent } from './components/blog/blog.component';
import { ArticleComponent } from './components/blog/article/article.component';
import { ProjectCardComponent } from './components/projects/project-card/project-card.component';


@NgModule({
  declarations: [
    AppComponent,
    ExperiencesComponent,
    TrainingsComponent,
    ResumeComponent,
    ResumeCardComponent,
    CircularSkillComponent,
    SkillsDetailsDialogComponent,
    ProjectsComponent,
    ProjectComponent,
    BlogComponent,
    ArticleComponent,
    ProjectCardComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
