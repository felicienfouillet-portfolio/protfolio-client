export interface SkillsDialogData {
    title: string;
    description: string;
}