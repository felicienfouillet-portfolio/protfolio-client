export interface IProject {
    _id: number;
    title: string;
    subtitle?: string;
    imageUri?: string;
    description?: string;
}