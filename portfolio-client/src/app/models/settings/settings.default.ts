import { Settings } from "./settings.model";

export const DefaultSettings: Settings = {
    general: {
        isDarkMode: {
            isEditable: true,
            value: true
        }
    },
    advanced: {

    },
}