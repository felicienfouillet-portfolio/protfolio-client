export enum SettingsType {
    GENERAL = 'general',
    ADVANCED = 'advanced'
}

export interface Settings {
    general: {
        [key: string]: {
            isEditable: boolean,
            value: any
        } | any;
    }
    advanced: {
        [key: string]: {
            isEditable: boolean,
            value: any
        } | any;
    }
}

export interface MediaScreenInfos {
    isMobile: boolean,
    isTablet: boolean,
    isLandscape: boolean,
    isDesktop: boolean
}