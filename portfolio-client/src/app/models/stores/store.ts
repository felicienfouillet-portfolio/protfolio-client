
export type StoreValue = any;
export type StoreObject<StoreValueType extends StoreValue> = {
    key: string;
    value: StoreValueType;
};

export abstract class Store<StoreValueType extends StoreValue> {
    private _storage: Record<string, StoreValueType>;

    public get size() {
        return Object.keys(this._storage).length;
    };

    public get values() {
        return Object.keys(this._storage).map((key) => ({
            key,
            value: this._storage[key],
        }));
    };

    constructor() {
        this._storage = {};
    }

    public set(key: string, value: StoreValueType): void {
        this._storage[key] = value;
    }

    public get(key: string): StoreValueType {
        if (!this.has(key)) {
            throw new Error(`${key} not found`);
        }

        return this._storage[key];
    }

    public has(key: string): boolean {
        return this._storage.hasOwnProperty(key);
    }

    public delete(key: string): void {
        delete this._storage[key];
    }

    public clear(): void {
        this._storage = {};
    }

    public abstract save(): void | Promise<void>;
    public abstract load(): void | Promise<void>;
}