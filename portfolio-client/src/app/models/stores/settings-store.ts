import { SettingsType } from "../settings/settings.model";
import { Store, StoreObject } from "./store";

export type SettingsStoreValue = any;

export class SettingsStore extends Store<SettingsStoreValue> {

    private readonly _key = 'settings';

    constructor() {
        super();
    }

    public getSettingsByType(settingsType: SettingsType): Array<StoreObject<SettingsStoreValue>> {
        return this.values.filter(el => el.key.includes(settingsType + '-')) as Array<StoreObject<SettingsStoreValue>>;
    }

    public save(): void {
        const storeToSave = this.values.filter(el => el.key.includes('general-') || el.key.includes('advanced-'));
        localStorage.setItem(this._key, JSON.stringify(storeToSave));
    }

    public load(): void {
        const recoveredStore: Array<StoreObject<SettingsStoreValue>> = JSON.parse(localStorage.getItem(this._key));

        if (recoveredStore) {
            Array.from(recoveredStore).forEach(property => {
                this.set(property.key, property.value);
            });
            return;
        }

        if (recoveredStore == (null || undefined)) {
            return;
        }

        throw new Error('Error while loading settings store');
    }
}